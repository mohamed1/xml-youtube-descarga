#!/usr/bin/python3

#
# Simple XML parser for YouTube XML channels
# Jesus M. Gonzalez-Barahona
# jgb @ gsyc.es
# SARO and SAT subjects (Universidad Rey Juan Carlos)
# 2020
#
# Produces a HTML document in standard output, with
# the list of videos on the channel
#
# How to get the XML document for a YouTube channel:
# https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg

from xml.dom.minidom import parse
import sys
import urllib.request

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <h1>Channel contents:</h1>
    <ul>
        {videos}
    </ul>
  </body>
</html>
"""


if __name__ == "__main__":
    #Extraemos identificador del canal de yotube correspondiente
    if len(sys.argv) < 2:
        print("Usage: python3 parser_yt <id>")
        print(" ")
        sys.exit(1)

    #contruimos url
    url = 'https://www.youtube.com/feeds/videos.xml?channel_id=' + sys.argv[1]
    #Obtenemos documento xml y lo abrimos
    youtube_xml = urllib.request.urlopen(url)
    with youtube_xml as file:
        fichero = parse(youtube_xml)
        videos_yt = fichero.getElementsByTagName('entry')
        for video in videos_yt:
            titles = video.getElementsByTagName('title')
            links = video.getElementsByTagName('link')
            for link in links:
                link = link.getAttribute('href')
                video = f"<li><a href='{link}'>{titles[0].firstChild.nodeValue}.</a></li>"
                print(video)
